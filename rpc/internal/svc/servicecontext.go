package svc

import (
	"database/sql"
	model "ecom_kg/user/model/sqlc"
	"ecom_kg/user/rpc/internal/config"
	"log"

	_ "github.com/lib/pq"
)

type ServiceContext struct {
	Config config.Config
	Store  model.Store
}

func NewServiceContext(c config.Config) *ServiceContext {
	conn, err := sql.Open("postgres", "postgresql://pgadmin:Secret@123@localhost:5432/ecom_kg?sslmode=disable")
	if err != nil {
		log.Fatal("cannot connect to db:", err)
	}

	return &ServiceContext{
		Config: c,
		Store:  model.NewStore(conn),
	}
}
