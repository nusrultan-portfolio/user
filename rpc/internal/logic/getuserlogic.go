package logic

import (
	"context"
	"database/sql"

	"ecom_kg/user/rpc/internal/svc"
	"ecom_kg/user/rpc/user"

	"github.com/zeromicro/go-zero/core/logx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type GetUserLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetUserLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetUserLogic {
	return &GetUserLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *GetUserLogic) GetUser(in *user.GetUserRequest) (*user.GetUserResponse, error) {
	// todo: add your logic here and delete this line

	getuser, err := l.svcCtx.Store.GetUser(l.ctx, in.Id)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, status.Errorf(codes.NotFound, "id (%v) does not exist", in.Id)
		}
		return nil, status.Errorf(codes.DeadlineExceeded, "%v", err)
	}

	return &user.GetUserResponse{
		Firstname: getuser.Firstname,
		Lastname:  getuser.Lastname,
		Username:  getuser.Username,
		Email:     getuser.Email,
		Password:  getuser.Password,
		CreatedAt: getuser.CreatedAt.String(),
		UpdatedAt: getuser.UpdatedAt.String(),
	}, nil
}
