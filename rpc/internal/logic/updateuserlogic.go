package logic

import (
	"context"
	"database/sql"
	"time"

	model "ecom_kg/user/model/sqlc"
	"ecom_kg/user/rpc/internal/svc"
	"ecom_kg/user/rpc/user"

	"github.com/zeromicro/go-zero/core/logx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type UpdateUserLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewUpdateUserLogic(ctx context.Context, svcCtx *svc.ServiceContext) *UpdateUserLogic {
	return &UpdateUserLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *UpdateUserLogic) UpdateUser(in *user.UpdateRequest) (*user.UpdateResponse, error) {
	// todo: add your logic here and delete this line
	arg := model.UpdateUserParams{
		ID:        in.Id,
		Firstname: in.Firstname,
		Lastname:  in.Lastname,
		Username:  in.Username,
		Email:     in.Email,
		Password:  in.Password,
		UpdatedAt: time.Now(),
	}

	updated, err := l.svcCtx.Store.UpdateUser(l.ctx, arg)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, status.Errorf(codes.NotFound, "id (%v) does not exist", in.Id)
		}
		return nil, status.Errorf(codes.DeadlineExceeded, "%v", err)
	}

	return &user.UpdateResponse{
		Id:        updated.ID,
		Firstname: updated.Firstname,
		Lastname:  updated.Lastname,
		Username:  updated.Username,
		Email:     updated.Email,
		Password:  updated.Password,
		CreatedAt: updated.CreatedAt.String(),
		UpdatedAt: updated.UpdatedAt.String(),
	}, nil
}
