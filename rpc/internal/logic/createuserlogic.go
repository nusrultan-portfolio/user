package logic

import (
	"context"

	model "ecom_kg/user/model/sqlc"
	"ecom_kg/user/rpc/internal/svc"
	"ecom_kg/user/rpc/user"

	"github.com/zeromicro/go-zero/core/logx"
)

type CreateUserLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewCreateUserLogic(ctx context.Context, svcCtx *svc.ServiceContext) *CreateUserLogic {
	return &CreateUserLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *CreateUserLogic) CreateUser(in *user.CreateUserRequest) (*user.CreateUserResponse, error) {
	// todo: add your logic here and delete this line

	arg := model.CreateUserParams{
		Firstname: in.Firstname,
		Lastname:  in.Lastname,
		Username:  in.Username,
		Email:     in.Email,
		Password:  in.Password,
	}

	us, err := l.svcCtx.Store.CreateUser(l.ctx, arg)
	if err != nil {
		logx.Error("db.CreateUser err:", err)
		return nil, err
	}

	return &user.CreateUserResponse{
		Id: us.ID,
	}, nil
}
