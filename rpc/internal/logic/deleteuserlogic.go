package logic

import (
	"context"
	"database/sql"
	"fmt"

	"ecom_kg/user/rpc/internal/svc"
	"ecom_kg/user/rpc/user"

	"github.com/zeromicro/go-zero/core/logx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type DeleteUserLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewDeleteUserLogic(ctx context.Context, svcCtx *svc.ServiceContext) *DeleteUserLogic {
	return &DeleteUserLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *DeleteUserLogic) DeleteUser(in *user.DeleteRequest) (*user.DeleteResponse, error) {
	// todo: add your logic here and delete this line

	id, err := l.svcCtx.Store.DeleteUser(l.ctx, in.Id)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, status.Errorf(codes.NotFound, "id (%v) does not exist", in.Id)
		}
		return nil, status.Errorf(codes.DeadlineExceeded, "%v", err)

	}

	return &user.DeleteResponse{
		Message: fmt.Sprintf("id (%v) deleted successfully", id),
	}, nil
}
