package logic

import (
	"context"
	"fmt"

	model "ecom_kg/user/model/sqlc"
	"ecom_kg/user/rpc/internal/svc"
	"ecom_kg/user/rpc/user"

	"github.com/zeromicro/go-zero/core/logx"
)

type ListUsersLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewListUsersLogic(ctx context.Context, svcCtx *svc.ServiceContext) *ListUsersLogic {
	return &ListUsersLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *ListUsersLogic) ListUsers(in *user.ListUsersRequest) (*user.ListUsersResponse, error) {
	// todo: add your logic here and delete this line

	res := (in.PageId - 1) * in.PageSize

	arg := model.ListUsersParams{
		Limit:  in.PageSize,
		Offset: res,
	}

	users, err := l.svcCtx.Store.ListUsers(l.ctx, arg)
	if err != nil {
		return nil, fmt.Errorf("listUsers users:%v err: %v", users, err)
	}

	listUsers := make([]user.User, len(users))

	for i := 0; i < len(users); i++ {
		listUsers[i].Id = users[i].ID
		listUsers[i].Firstname = users[i].Firstname
		listUsers[i].Lastname = users[i].Lastname
		listUsers[i].Username = users[i].Username
		listUsers[i].Password = users[i].Password
		listUsers[i].CreatedAt = users[i].CreatedAt.String()
		listUsers[i].UpdatedAt = users[i].UpdatedAt.String()
	}

	resUsers := make([]*user.User, 0)
	for i := 0; i < len(listUsers); i++ {
		resUsers = append(resUsers, &listUsers[i])
	}

	return &user.ListUsersResponse{
		Users: resUsers,
	}, nil
}
